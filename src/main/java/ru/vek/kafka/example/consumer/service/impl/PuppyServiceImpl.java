package ru.vek.kafka.example.consumer.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vek.kafka.example.consumer.dto.PuppyDto;
import ru.vek.kafka.example.consumer.mapper.PuppyMapper;
import ru.vek.kafka.example.consumer.repository.PuppyRepository;
import ru.vek.kafka.example.consumer.service.PuppyService;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class PuppyServiceImpl implements PuppyService {

    private final PuppyMapper mapper;
    @Autowired
    private PuppyRepository repository;

    @Override
    public void addNewPuppyToWishList(PuppyDto dto) {
        repository.save(mapper.convertToEntity(dto));
    }

    @Override
    public List<PuppyDto> getAllPuppies() {
        return mapper.convertToDtoList(repository.findAll());
    }


}
