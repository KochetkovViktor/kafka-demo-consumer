package ru.vek.kafka.example.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vek.kafka.example.consumer.entity.PuppyEntity;


public interface PuppyRepository extends JpaRepository<PuppyEntity, Long> {
}
