package ru.vek.kafka.example.consumer.service;

import ru.vek.kafka.example.consumer.dto.PuppyDto;

import java.util.List;

public interface PuppyService {

    void addNewPuppyToWishList(PuppyDto dto);

    List<PuppyDto> getAllPuppies();
}
